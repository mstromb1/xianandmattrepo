//
//  GameScene.h
//  SpaceInvader
//

//  Copyright (c) 2016 Xian Wu. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene <SKPhysicsContactDelegate>

@end
