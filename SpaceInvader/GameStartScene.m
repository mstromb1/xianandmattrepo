//
//  GameStartScene.m
//  SpaceInvader
//
//  Created by Xian Wu on 2/22/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//

#import "GameStartScene.h"
#import "GameScene.h"

IB_DESIGNABLE
@implementation GameStartScene
@synthesize b1,b2;
- (void)viewDidLoad
{
    //[super viewDidLoad];
    // Pause the view (and thus the game) when the app is interrupted or backgrounded    
    // Configure the view.
    UIImageView *bgImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"backgroundIMG"]];
    bgImage.frame = self.view.frame;
    bgImage.center = self.view.center;
    [self.view addSubview:bgImage];
    [self.view sendSubviewToBack:bgImage];
    //b1.layer.borderWidth = 3.0f;
//    [b1 setTitle:<#(nullable NSString *)#> forState:UIControlStateNormal]
    [b1.titleLabel setFont:[UIFont fontWithName:@"Chalkduster" size:36]];
    [b1 setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
    //b2 = [[UIButton alloc]initWithFrame:CGRectMake(b1.center.x, b1.center.y+100, 60, 40)];
    [b2 setTitle:@"Help" forState:UIControlStateNormal];
    [b2.titleLabel setFont:[UIFont fontWithName:@"Chalkduster" size:36]];
    [b2 setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
    
    //b1.layer.borderColor = [UIColor whiteColor].CGColor;
    
}

-(IBAction)helperF:(id)sender{
    UILabel *l1 = [[UILabel alloc]initWithFrame:CGRectMake(self.view.center.x-150, self.view.center.y-300, 300, 300)];
    [l1 setText:@"Press the left side to move the spaceship to left\npress the right side to move the spaceship to right\npress anyother place would fire~\nEvery 5 enemies down you will have a super cannon"];
    l1.lineBreakMode = NSLineBreakByWordWrapping;
    l1.numberOfLines = 0;
    [l1 setFont:[UIFont fontWithName:@"Chalkduster" size:18]];
    [l1 setTextColor:[UIColor whiteColor]];
    //[l1 setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:l1];
    [self.view bringSubviewToFront:l1];
}

@end
