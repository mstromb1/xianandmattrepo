//
//  GameStartScene.h
//  SpaceInvader
//
//  Created by Xian Wu on 2/22/16.
//  Copyright © 2016 Xian Wu. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameStartScene : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *b1,*b2;
@end
